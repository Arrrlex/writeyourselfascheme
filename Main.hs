module Main where
import           System.Environment

main :: IO ()
main = main3

main0 = do
  args <- getArgs
  putStrLn $ "Hello, " ++ args !! 0

main1 = do
  args <- getArgs
  putStrLn $ "Hello, " ++ args !! 0 ++ args !! 1

main2 = do
  args <- getArgs
  let x = args !! 0
  let y = args !! 1
  putStrLn $ x ++ " + " ++ y ++ " = " ++ show (read x + read y)


main3 = putStrLn "Hello! What is your name?" >> getLine >>= putStrLn . ("Hello, " ++)
--main3 = do
--  putStrLn $ "Hello! What is your name?"
--  line <- getLine
--  putStrLn $ "Hello, " ++ line
