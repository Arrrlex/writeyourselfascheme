.PHONY: hello parser eval
hello:
	ghc -o hello --make Main.hs
parser:
	ghc -o parser --make Parsing.hs
eval:
	ghc -o eval --make Eval.hs
