module Main where
import           Text.ParserCombinators.Parsec
                                         hiding ( spaces )
import           Text.Parsec.Combinator         ( parserTraced )
import           System.Environment
import           Control.Monad
import           Data.Char                      ( digitToInt )
import           Data.Maybe                     ( listToMaybe )
import           Numeric                        ( readInt
                                                , readOct
                                                , readHex
                                                )
import Debug.Trace
main :: IO ()
main = do
  arg <- head <$> getArgs
  putStrLn $ readExpr arg

data LispVal
        = Atom String
 | List [LispVal]
 | DottedList [LispVal] LispVal
 | Number Integer
 | String String
 | Bool Bool
 deriving Show

readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
  Left  err -> "No match " ++ show err
  Right val -> "Found value " ++ show val


parseExpr :: Parser LispVal
parseExpr =
  parseNumber <|> parseAtom <|> parseString <|> parseQuoted <|> parseAnyList

parseString :: Parser LispVal
parseString = do
  char '"'
  x <- many
    (noneOf "\"" <|> quote <|> newline <|> carriagertn <|> tab <|> backslash)
  char '"'
  pure $ String x
 where
  quote       = string "\\\"" *> pure '"'
  newline     = string "\\\n" *> pure '\n'
  carriagertn = string "\\\r" *> pure '\r'
  tab         = string "\\\t" *> pure '\t'
  backslash   = string "\\\\" *> pure '\\'


parseBool :: Parser LispVal
parseBool =
  (string "#t" *> pure (Bool True)) <|> (string "#f" *> pure (Bool False))

parseAtom :: Parser LispVal
parseAtom = do
  first <- letter <|> symbol
  rest  <- many (letter <|> symbol <|> digit)
  pure $ Atom $ first : rest

parseNumber :: Parser LispVal
parseNumber = do
  x <- parsePrefix <*> many1 digit
  case x of
    Just num -> pure $ Number num
    Nothing  -> fail "Unable to read number"
 where
  parsePrefix :: Parser (String -> Maybe Integer)
  parsePrefix =
    (string "#b" *> pure readBinary)
      <|> (string "#o" *> pure readOctal)
      <|> (string "#h" *> pure readHexadecimal)
      <|> pure (Just . read)


parseAnyList :: Parser LispVal
parseAnyList = do
  char '('
  head <- sepEndBy parseExpr spaces
  tail <- optionMaybe (char '.' *> spaces *> parseExpr)
  char ')'
  case tail of
    Just x  -> pure $ DottedList head x
    Nothing -> pure $ List head

parseQuoted :: Parser LispVal
parseQuoted = do
  symbol <- quote <|> quasiquote <|> unquote <|> unquoteSplicing
  x      <- parseExpr
  pure $ List [Atom symbol, x]
 where
  quote           = char '\'' *> pure "quote"
  quasiquote      = char '`' *> pure "quasiquote"
  unquote         = char ',' *> pure "unquote"
  unquoteSplicing = string ",@" *> pure "unquote-splicing"


-- Helper parsers
symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

spaces :: Parser ()
spaces = skipMany1 space

-- Other functions
readBinary :: Integral a => String -> Maybe a
readBinary = fmap fst . listToMaybe . readInt 2 (`elem` "01") digitToInt

readOctal :: Integral a => String -> Maybe a
readOctal = fmap fst . listToMaybe . readOct

readHexadecimal :: Integral a => String -> Maybe a
readHexadecimal = fmap fst . listToMaybe . readHex
