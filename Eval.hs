module Main where
import           Text.ParserCombinators.Parsec
                                         hiding ( spaces )
import           Text.Parsec.Combinator         ( parserTraced )
import           System.Environment
import           Control.Monad
import           Data.Char                      ( digitToInt )
import           Data.Maybe                     ( listToMaybe )
import           Numeric                        ( readInt
                                                , readOct
                                                , readHex
                                                )
import           Debug.Trace
main :: IO ()
main = getArgs >>= print . eval . readExpr . head

data LispVal
        = Atom String
 | List [LispVal]
 | DottedList [LispVal] LispVal
 | Number Integer
 | String String
 | Bool Bool

showVal :: LispVal -> String
showVal (String contents) = "\"" ++ contents ++ "\""
showVal (Atom   name    ) = name
showVal (Number contents) = show contents
showVal (Bool   True    ) = "#t"
showVal (Bool   False   ) = "#f"
showVal (List   contents) = "(" ++ unwordsList contents ++ ")"
showVal (DottedList head tail) =
  "(" ++ unwordsList head ++ " . " ++ showVal tail ++ ")"

unwordsList :: [LispVal] -> String
unwordsList = unwords . map showVal

instance Show LispVal where
  show = showVal

readExpr :: String -> LispVal
readExpr input = case parse parseExpr "lisp" input of
  Left  err -> String $ "No match " ++ show err
  Right val -> val


parseExpr :: Parser LispVal
parseExpr =
  parseNumber <|> parseAtom <|> parseString <|> parseQuoted <|> parseAnyList

parseString :: Parser LispVal
parseString = do
  char '"'
  x <- many
    (noneOf "\"" <|> quote <|> newline <|> carriagertn <|> tab <|> backslash)
  char '"'
  pure $ String x
 where
  quote       = string "\\\"" *> pure '"'
  newline     = string "\\\n" *> pure '\n'
  carriagertn = string "\\\r" *> pure '\r'
  tab         = string "\\\t" *> pure '\t'
  backslash   = string "\\\\" *> pure '\\'


parseBool :: Parser LispVal
parseBool =
  (string "#t" *> pure (Bool True)) <|> (string "#f" *> pure (Bool False))

parseAtom :: Parser LispVal
parseAtom = do
  first <- letter <|> symbol
  rest  <- many (letter <|> symbol <|> digit)
  pure $ Atom $ first : rest

parseNumber :: Parser LispVal
parseNumber = do
  x <- parsePrefix <*> many1 digit
  case x of
    Just num -> pure $ Number num
    Nothing  -> fail "Unable to read number"
 where
  parsePrefix :: Parser (String -> Maybe Integer)
  parsePrefix =
    (string "#b" *> pure readBinary)
      <|> (string "#o" *> pure readOctal)
      <|> (string "#h" *> pure readHexadecimal)
      <|> pure (Just . read)


parseAnyList :: Parser LispVal
parseAnyList = do
  char '('
  head <- sepEndBy parseExpr spaces
  tail <- optionMaybe (char '.' *> spaces *> parseExpr)
  char ')'
  case tail of
    Just x  -> pure $ DottedList head x
    Nothing -> pure $ List head

parseQuoted :: Parser LispVal
parseQuoted = do
  symbol <- quote <|> quasiquote <|> unquote <|> unquoteSplicing
  x      <- parseExpr
  pure $ List [Atom symbol, x]
 where
  quote           = char '\'' *> pure "quote"
  quasiquote      = char '`' *> pure "quasiquote"
  unquote         = char ',' *> pure "unquote"
  unquoteSplicing = string ",@" *> pure "unquote-splicing"


-- Helper parsers
symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

spaces :: Parser ()
spaces = skipMany1 space

-- Other functions
readBinary :: Integral a => String -> Maybe a
readBinary = fmap fst . listToMaybe . readInt 2 (`elem` "01") digitToInt

readOctal :: Integral a => String -> Maybe a
readOctal = fmap fst . listToMaybe . readOct

readHexadecimal :: Integral a => String -> Maybe a
readHexadecimal = fmap fst . listToMaybe . readHex


eval :: LispVal -> LispVal
eval val@(String _                  ) = val
eval val@(Number _                  ) = val
eval val@(Bool   _                  ) = val
eval (    List   [Atom "quote", val]) = val
eval (    List   (Atom func : args) ) = apply func $ map eval args

apply :: String -> [LispVal] -> LispVal
apply func args = maybe (Bool False) ($ args) $ lookup func primitives

primitives :: [(String, [LispVal] -> LispVal)]
primitives =
  [ ("+"        , numericBinOp (+))
  , ("-"        , numericBinOp (-))
  , ("*"        , numericBinOp (*))
  , ("/"        , numericBinOp div)
  , ("mod"      , numericBinOp mod)
  , ("quotient" , numericBinOp quot)
  , ("remainder", numericBinOp rem)
  , ("symbol?", typeCheck isSymbol)
  , ("boolean?", typeCheck isBoolean)
  , ("number?", typeCheck isNumber)
  , ("string?", typeCheck isString)
  , ("pair?", typeCheck isPair)
  , ("list?", typeCheck isList)
  , ("symbol->string", symbolToString)
  , ("string->symbol", stringToSymbol)
  ]

typeCheck :: (LispVal -> Bool) -> [LispVal] -> LispVal
typeCheck f [val] = Bool $ f val
typeCheck f _ = Bool False

isBoolean :: LispVal -> Bool
isBoolean (Bool _) = True
isBoolean _ = False

isSymbol:: LispVal -> Bool
isSymbol (Atom _) = True
isSymbol _ = False

isNumber :: LispVal -> Bool 
isNumber (Number _) = True
isNumber _ = False

isString :: LispVal -> Bool
isString (String _) = True
isString _ = False

isPair :: LispVal -> Bool
isPair (DottedList _ _) = True
isPair _ = False

isList :: LispVal -> Bool
isList (List _) = True
isList _ = False

numericBinOp :: (Integer -> Integer -> Integer) -> [LispVal] -> LispVal
numericBinOp op params = Number $ foldl1 op $ map unpackNum params

unpackNum :: LispVal -> Integer
unpackNum (Number n) = n
unpackNum _          = 0

symbolToString :: [LispVal] -> LispVal
symbolToString [Atom v] = String v
symbolToString _ = Bool False

stringToSymbol :: [LispVal] -> LispVal
stringToSymbol [String v] = Atom v
stringToSymbol _ = Bool False
